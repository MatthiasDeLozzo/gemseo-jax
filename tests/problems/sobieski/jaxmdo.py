from __future__ import annotations

import jax
from gemseo import configure_logger
from gemseo.core.mdo_scenario import MDOScenario
from gemseo.problems.sobieski.core.design_space import SobieskiDesignSpace
from gemseo_jax.problems.sobieski.chain import JAXSobieskiChain

configure_logger()
jax.config.update("jax_enable_x64", True)

chain = JAXSobieskiChain()
discipline = chain.to_discipline()
scenario = MDOScenario(
    [discipline], "MDF", "y_4", SobieskiDesignSpace(), maximize_objective=True
)
scenario.add_constraint("g_1", "ineq")
scenario.add_constraint("g_2", "ineq")
scenario.add_constraint("g_3", "ineq")
scenario.execute({"algo": "SLSQP", "max_iter": 100})
