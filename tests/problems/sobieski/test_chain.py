# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
"""Tests for JAXSobieskiChain."""

from __future__ import annotations

import jax
from gemseo_jax.problems.sobieski.chain import JAXSobieskiChain
from numpy import atleast_1d
from numpy.testing import assert_allclose

from gemseo.core.chain import MDOChain
from gemseo.problems.sobieski.core.problem import SobieskiProblem
from gemseo.problems.sobieski.disciplines import SobieskiAerodynamics
from gemseo.problems.sobieski.disciplines import SobieskiMission
from gemseo.problems.sobieski.disciplines import SobieskiPropulsion
from gemseo.problems.sobieski.disciplines import SobieskiStructure

jax.config.update("jax_enable_x64", True)


def test_execute():
    """Check the execution of JAXSobieskiChain."""
    jax_chain = JAXSobieskiChain()
    input_data = SobieskiProblem().get_default_inputs(jax_chain.input_grammar.names)
    mdo_chain = MDOChain([
        SobieskiAerodynamics(),
        SobieskiStructure(),
        SobieskiPropulsion(),
        SobieskiMission(),
    ])
    expected_output_data = mdo_chain.execute(input_data)
    output_data = {k: atleast_1d(v) for k, v in jax_chain.execute(input_data).items()}
    for name in output_data:
        assert_allclose(output_data[name], expected_output_data[name])
