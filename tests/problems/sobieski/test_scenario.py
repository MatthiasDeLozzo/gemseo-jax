# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
"""Tests for the Sobieski's Supersonic Business Jet MDO benchmark written in JAX."""

from __future__ import annotations

import jax
from gemseo_jax.problems.sobieski.chain import JAXSobieskiChain
from numpy.testing import assert_allclose

from gemseo import configure_logger
from gemseo.core.doe_scenario import DOEScenario
from gemseo.problems.sobieski.core.design_space import SobieskiDesignSpace
from gemseo.problems.sobieski.disciplines import create_disciplines

jax.config.update("jax_enable_x64", True)


def test_solution():
    """Compare the JAX-based solution with the NumPy-based one."""
    configure_logger()

    chain = JAXSobieskiChain()
    n_samples = 100
    scenario = DOEScenario(
        [chain],
        "MDF",
        "y_4",
        SobieskiDesignSpace(),
        maximize_objective=True,
        grammar_type=DOEScenario.GrammarType.SIMPLER,
    )
    scenario.add_constraint("g_1", "ineq")
    scenario.add_constraint("g_2", "ineq")
    scenario.add_constraint("g_3", "ineq")
    scenario.execute({"algo": "OT_HALTON", "n_samples": n_samples})
    jax_dataset = scenario.to_dataset()

    scenario = DOEScenario(
        create_disciplines(),
        "MDF",
        "y_4",
        SobieskiDesignSpace(),
        maximize_objective=True,
    )
    scenario.add_constraint("g_1", "ineq")
    scenario.add_constraint("g_2", "ineq")
    scenario.add_constraint("g_3", "ineq")
    scenario.execute({"algo": "OT_HALTON", "n_samples": n_samples})
    numpy_dataset = scenario.to_dataset()

    v1 = jax_dataset.get_view(variable_names="-y_4").to_numpy()
    v2 = numpy_dataset.get_view(variable_names="-y_4").to_numpy()
    assert_allclose(v1, v2, rtol=1e-3)
