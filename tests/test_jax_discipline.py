# Copyright 2023 ISAE-SUPAERO, https://www.isae-supaero.fr/en/
# Copyright 2023 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""Test functions for the JAXDiscipline."""

from __future__ import annotations

from typing import TYPE_CHECKING
from typing import Callable

import pytest
from gemseo import configure_logger
from jax.config import config
from numpy import array
from numpy.testing import assert_equal

from gemseo_jax.jax_discipline import JAXDiscipline
from gemseo_jax.jax_discipline import NumberLike

if TYPE_CHECKING:
    from collections.abc import Mapping

# Import this if prints are uncommented from test_jacobian_diff_inouts
# from jax import make_jaxpr

config.update("jax_enable_x64", True)


@pytest.fixture(scope="module")
def function() -> Callable[[Mapping[str, NumberLike]], Mapping[str, NumberLike]]:
    """A function computing linear combinations."""

    def my_function(input_data: Mapping[str, NumberLike]):
        """A function computing linear combinations.

        Args:
            input_data: The input data.

        Returns:
            The output data.
        """
        a = input_data["a"]
        b = input_data["b"]
        c = input_data["c"]
        d = input_data["d"]

        x = 2 * a + b - c * d
        y = a * b
        z = c * d
        return {"x": x, "y": y, "z": z}

    return my_function


@pytest.fixture(scope="module")
def input_names() -> list[str]:
    """The input names."""
    return ["a", "b", "c", "d"]


@pytest.fixture(scope="module")
def output_names() -> list[str]:
    """The output names."""
    return ["x", "y", "z"]


@pytest.fixture(scope="module")
def default_inputs() -> dict[str, float]:
    """The default input values."""
    return {"a": 0.0, "b": 1.0, "c": 2.0, "d": 3.0}


def test_init(function, input_names, output_names, default_inputs):
    """Test JAXDiscipline object initialization."""
    discipline = JAXDiscipline(function, input_names, output_names, default_inputs)
    assert discipline is not None
    assert set(discipline.input_grammar) == set(input_names)
    assert set(discipline.output_grammar) == set(output_names)
    for input_name in input_names:
        assert_equal(discipline.default_inputs[input_name], default_inputs[input_name])


def test_execution(function, input_names, output_names, default_inputs):
    """Test JAXDiscipline execution."""
    discipline = JAXDiscipline(function, input_names, output_names, default_inputs)
    output_data = discipline.execute()
    default_outputs = function(default_inputs)

    for output_name in output_names:
        assert_equal(output_data[output_name], default_outputs[output_name])


def test_jacobian(function, input_names, output_names, default_inputs):
    """Test JAXDisipline jacobian computation."""
    discipline = JAXDiscipline(function, input_names, output_names, default_inputs)
    discipline.execute()
    assert discipline.check_jacobian()


def test_execution_pre_run(function, input_names, output_names, default_inputs):
    """Test JAXDiscipline execution with pre-run compilation."""
    discipline = JAXDiscipline(function, input_names, output_names, default_inputs)
    discipline.compile_jit(pre_run=True)

    default_outputs = function(default_inputs)
    output_data = discipline.execute()
    assert all(output_data[var] == default_outputs[var] for var in output_names)


def test_jacobian_diff_inouts(function, input_names, output_names, default_inputs):
    """Test JAXDiscipline execution."""
    configure_logger()
    discipline = JAXDiscipline(function, input_names, output_names, default_inputs)
    discipline.compile_jit()
    assert discipline.check_jacobian()
    # print("JACOBIAN COMPUTATION GRAPH:")
    # print("before filtering: -------------------------------------------------------")
    # print(make_jaxpr(discipline._jax_jac_func)(default_inputs))

    discipline.add_differentiated_inputs(["c", "d"])
    discipline.add_differentiated_outputs(["z"])
    discipline.compile_jit()
    # print()
    # print("after filtering: ------------------------------------------------------- ")
    # print(make_jaxpr(discipline._jax_jac_func)(default_inputs))
    assert discipline.check_jacobian(inputs=["c", "d"], outputs=["z"])

    # Check at different point with jacobian filter
    assert discipline.check_jacobian(
        input_data={
            "a": array([10.0]),
            "b": array([10.0]),
            "c": array([10.0]),
            "d": array([10.0]),
        },
        # todo: use float only once .check_jacobian typing is consistent with .execute
        inputs=["c", "d"],
        outputs=["z"],
    )
