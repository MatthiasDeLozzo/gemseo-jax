# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
"""Base discipline for the Sobieski's SSBJ use case."""

from __future__ import annotations

from abc import abstractmethod
from typing import TYPE_CHECKING
from typing import ClassVar

from gemseo_jax.auto_jax_discipline import AutoJAXDiscipline
from jax.numpy import array
from jax.numpy import clip

from gemseo.problems.sobieski.core.problem import SobieskiProblem
from gemseo.problems.sobieski.core.utils import SobieskiBase

if TYPE_CHECKING:
    from collections.abc import Sequence

    from jax import Array


class BaseJAXSobieskiDiscipline(AutoJAXDiscipline):
    """Base discipline for the Sobieski's SSBJ use case."""

    _NAME: ClassVar[str]
    """The name of the discipline."""

    DataType = SobieskiBase.DataType

    def __init__(
        self,
        jitted: bool = True,
        dtype: DataType = DataType.FLOAT,
    ) -> None:
        """
        Args:
             dtype: The data type.
        """  # noqa: D205 D212 D415
        super().__init__(self._jax_func, name=self._NAME)
        self.default_inputs = SobieskiProblem().get_default_inputs(
            self.get_input_data_names()
        )
        base = SobieskiBase(dtype)
        self.constants = base.constants
        self.dtype = base.dtype
        self._coeff_mtrix = array(
            [
                [0.2736, 0.3970, 0.8152, 0.9230, 0.1108],
                [0.4252, 0.4415, 0.6357, 0.7435, 0.1138],
                [0.0329, 0.8856, 0.8390, 0.3657, 0.0019],
                [0.0878, 0.7248, 0.1978, 0.0200, 0.0169],
                [0.8955, 0.4568, 0.8075, 0.9239, 0.2525],
            ],
            dtype=self.dtype,
        )
        (
            self.x_initial,
            self.tc_initial,
            self.half_span_initial,
            self.aero_center_initial,
            self.cf_initial,
            self.mach_initial,
            self.h_initial,
            self.throttle_initial,
            self.lift_initial,
            self.twist_initial,
            self.esf_initial,
        ) = base.get_initial_values()

    @abstractmethod
    def _jax_func(*args: Sequence[float]) -> float: ...

    def _compute_polynomial_coefficients(
        self, f_bound: Array, s_bound: Array
    ) -> tuple[Array, Array, Array]:
        """Compute the polynomial coefficients."""
        size = len(f_bound)
        a0 = 0
        ai = [None] * size
        aij = [[None] * size for _ in range(size)]
        for index in range(size):
            f_bound_i = f_bound[index]
            mtx_shifted = self.__compute_mtx_shifted(s_bound[index])
            a0 = f_bound_i[1]
            ai[index] = -(f_bound_i[2] - f_bound_i[0]) / (2 * mtx_shifted[0, 1])
            aij[index][index] = (
                f_bound_i[0] - f_bound_i[1] + (f_bound_i[2] - f_bound_i[0]) / 2
            ) / mtx_shifted[0, 2]

        for i in range(size):
            for j in range(i + 1, size):
                aij[i][j] = aij[i][i] * self._coeff_mtrix[i, j]
                aij[j][i] = aij[i][j]

        return (
            a0,
            array(ai, dtype=self.dtype),
            array(aij, dtype=self.dtype) * 0.5,
        )

    def _compute_polynomial_approximation(
        self, s_ref: Array, s_new: Array, a0: float, ai: Array, aij: Array
    ) -> Array:
        """Compute the polynomial coefficients.

        These coefficients characterize
        the behavior of certain synthetic variables and function modifiers.

        Args:
            s_ref: The initial values of the independent variables (5 variables at max).
            s_new: The current values of the independent variables.

        Returns:
            The value of the synthetic variables or function modifiers.
        """
        s_shifted = clip(s_new / s_ref, 0.75, 1.25) - 1.0
        return a0 + ai @ s_shifted + s_shifted.T @ aij @ s_shifted

    def __compute_mtx_shifted(self, s_bound: Array) -> Array:
        """Compute shifted mtx.

        Args:
            s_bound: s bound.

        Returns:
            The shifted mtx.
        """
        mtx_shifted = [
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
        ]
        mtx_shifted[0][1] = -s_bound
        mtx_shifted[2][1] = s_bound
        mtx_shifted[0][2] = mtx_shifted[2][2] = s_bound**2
        return array(mtx_shifted, dtype=self.dtype)
