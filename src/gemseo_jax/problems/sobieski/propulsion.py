# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
"""Propulsion discipline for the Sobieski's SSBJ use case."""
from __future__ import annotations

from typing import Sequence

from jax import Array
from jax.numpy import array
from numpy import array as np_array

from gemseo_jax.problems.sobieski.base import BaseJAXSobieskiDiscipline


class JAXSobieskiPropulsion(BaseJAXSobieskiDiscipline):
    """Propulsion discipline for the Sobieski's SSBJ use case."""

    _NAME = "SobieskiPropulsion"
    ESF_UPPER_LIMIT = 1.5
    ESF_LOWER_LIMIT = 0.5
    TEMPERATURE_LIMIT = 1.02

    def __init__(  # noqa: D107
        self,
        jitted: bool = True,
        dtype: BaseJAXSobieskiDiscipline.DataType = BaseJAXSobieskiDiscipline.DataType.FLOAT,
    ) -> None:
        super().__init__(jitted=jitted, dtype=dtype)
        self.__s_initial = array(
            [self.mach_initial, self.h_initial, self.throttle_initial], dtype=self.dtype
        )
        self.__flag_temp = array(
            [[0.95, 1.0, 1.1], [1.05, 1.0, 0.9], [0.95, 1.0, 1.1]], dtype=self.dtype
        )
        self.__bound_temp = array([0.25, 0.25, 0.25], dtype=self.dtype)
        self.throttle_coeff = self.dtype(16168.6)
        self.sfc_coeff = array(
            [
                1.13238425638512,
                1.53436586044561,
                -0.00003295564466,
                -0.00016378694115,
                -0.31623315541888,
                0.00000410691343,
                -0.00005248000590,
                -0.00000000008574,
                0.00000000190214,
                0.00000001059951,
            ],
            dtype=self.dtype,
        )
        self.thua_coeff = array(
            [
                11483.7822254806,
                10856.2163466548,
                -0.5080237941,
                3200.157926969,
                -0.1466251679,
                0.0000068572,
            ],
            dtype=self.dtype,
        )
        self.default_inputs["c_3"] = np_array([self.constants[3]])
        self.a0_g31 = 1.0
        self.ai_g31 = array([0.3, -0.3, 0.3], dtype=self.dtype)
        self.aij_g31 = array(
            [
                [0.2, 0.0794, 0.16304],
                [0.0794, -0.2, -0.12714],
                [0.16304, -0.12714, 0.2],
            ],
            dtype=self.dtype,
        )

    def _jax_func(
        self,
        x_shared: Sequence[float],
        y_23: Sequence[float],
        x_3: Sequence[float],
        c_3: Sequence[float],
    ) -> tuple[Array, Array, Array, Array, Array]:
        altitude = x_shared[1]
        mach = x_shared[2]
        adim_throttle = x_3[0]
        drag = y_23[0]
        c_3 = c_3[0]

        throttle = adim_throttle * self.throttle_coeff
        y_34 = (
            self.sfc_coeff[0]
            + self.sfc_coeff[1] * mach
            + self.sfc_coeff[2] * altitude
            + self.sfc_coeff[3] * throttle
            + self.sfc_coeff[4] * mach**2
            + 2 * altitude * mach * self.sfc_coeff[5]
            + 2 * throttle * mach * self.sfc_coeff[6]
            + self.sfc_coeff[7] * altitude**2
            + 2 * throttle * altitude * self.sfc_coeff[8]
            + self.sfc_coeff[9] * throttle**2
        )
        g_30 = y_32 = drag / (3.0 * throttle)
        y_31 = c_3 * (y_32**1.05) * 3
        y_3 = array([y_34, y_31, y_32], dtype=self.dtype)

        # jax.debug.print("{}", g_3)
        g_31 = self._compute_polynomial_approximation(
            self.__s_initial,
            array([mach, altitude, adim_throttle], dtype=self.dtype),
            self.a0_g31,
            self.ai_g31,
            self.aij_g31,
        )
        throttle_ua = (
            self.thua_coeff[0]
            + self.thua_coeff[1] * mach
            + self.thua_coeff[2] * altitude
            + self.thua_coeff[3] * mach**2
            + 2 * self.thua_coeff[4] * mach * altitude
            + self.thua_coeff[5] * altitude**2
        )
        g_32 = throttle / throttle_ua - 1.0

        g_3 = array(
            [
                g_30 - self.ESF_UPPER_LIMIT,
                self.ESF_LOWER_LIMIT - g_30,
                g_32,
                g_31 - self.TEMPERATURE_LIMIT,
            ],
            dtype=self.dtype,
        )

        return y_3, y_34, y_31, y_32, g_3
